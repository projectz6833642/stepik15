import unittest
from unittest.mock import patch
from io import StringIO
from main import foo

class test_funkcji_foo(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    def test_foo_printuj_arg(self, mock_stdout):
        expected_output = "hello\n"
        foo("hello")
        self.assertEqual(mock_stdout.getvalue(), expected_output)

if __name__ == '__main__':
    unittest.main()
